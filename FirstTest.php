<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use App\Entity\User\User;
use App\Entity\Misc\Template;
use App\Entity\Misc\Room;



class FirstTest extends Controller {
	private $Visited=array();
    /**
     * @Route("/" , name="default")
     */
    public function gentelTest(Request $request) {
		$Selectquery=null;
		$Output=array();
		$rel=array();
		$rows=array();
		$order=array();
		$lookup=$this->actionLookUpTable();
		$loadingTemplate=$this->load();
		$SelectedArray=array();
		$Attributes=array();
		$group=array();
		$Results=array();
		$Count=array();

		if($request->getMethod()=='POST'){
		
			//'GET','POST'
		
			$entities = $request->request->get('Entities');
			if($request->request->get('GroupBy')!=Null){
			foreach($entities as $e)
			{
				$Attributes = $request->request->get($e.'_Attributes');
				$SelectedArray[$e]=array();	
				foreach($Attributes as $a)
				{
					if(!empty($lookup[$e][$a])){
					$SelectedArray[$e][$a]=$lookup[$e][$a]['TargetEntity'];
					}
				}
			}
		
			foreach($SelectedArray as $e=> $Attr)
			{
					foreach($Attr as $a=>$target)
					{
						if(in_array($e,$SelectedArray[$target]))
						{
							$key=array_search($e,$SelectedArray[$target]);
							unset($SelectedArray[$target][$key]);	
						}
					}
			}
			$Sorted=$this->topologicalsort($SelectedArray,$entities);
			
			$group=$request->request->get('GroupBy');
			$conn= $this->getDoctrine()->getManager()->getConnection();
			$qb=$conn->createQueryBuilder();
			foreach($Sorted as $entity)
			{
				$Attr=$SelectedArray[$entity];

				//$conn= $this->getDoctrine()->getManager()->getConnection();
				
				foreach($Attr as $A){
				$qb=$this->addSelect($A,$entity.'.$A')
						 ->from($entity,$entity);
				}
				foreach($Attr as $a=>$target)
				{
					$qb=$this->groups($qb,$a,$SelectedArray,$group, $entity,$entity);				
				}
				$Output=$qb->execute()->fetchAll();	
			}
			foreach($Output as $query){
				$Results[]=array(
					'entity'=>$query,
					'attributes'=>$Attr,
					);	
			}
			}
			else
			{
				if(!($request->request->get('Aggregation')))
				{
					foreach($entities as $entity)
						{
							
						/*foreach($this->getRelatedEntities($this->Metadata($entity)) as $r) {
						$rel[] = $r;
						}*/
						$Attributes = $request->request->get($entity.'_Attributes');
						$Selectquery = $this->getDoctrine()
											->getRepository($this->Metadata($entity))
											->findAll();
											/*->createQueryBuilder()
											->join('e1.'.attribute,'a1');*/
											
					foreach($Selectquery as $query)
						{
							$Results[]=array(
							'entity'=>$query,
							'attributes'=>$Attributes,
							);
						}
					}
				}
				else if(($request->request->get('Aggregation'))=='Count')
				{
					$em = $this->getDoctrine()->getManager();
					
					$query = $em->createQuery('SELECT SUM(r.number), r.number FROM App\Entity\Misc\Room r GROUP BY r.number');
					
					$qb=$query->getResult();
					
					
					
						$Count[]=array(
							'Count'=>$qb,
							
					);
					
				}
				 else if(($request->request->get('Aggregation'))=='Sum')
				 {
					$em = $this->getDoctrine()->getManager();
					
					$query = $em->createQuery('SELECT SUM(r.capacity), r.number FROM App\Entity\Misc\Room r GROUP BY r.number');
					
					$qb=$query->getResult();
					$Count[]=array(
						'Count'=>$qb,
						
				);
			}
				// 	$conn=$this->getDoctrine()->getManager();
				// 	$queryBuilder=$conn->createQueryBuilder();
				// 	$qb=$this->select('','SUM()')
				// 			 ->from('','')
				// 			 ->getSingleScalarResult();
				// 	$qb=$this->getQuery()->getResults();
				// 	$Results[]=array(
				// 		'entity'=>$qb,					
				// 	);
				// }
				// else 
				// {
				// 	$queryAvgScore = $queryScore->createQueryBuilder('g')
				// 								->select("avg(g.score) as score_avg, count(g.score) as score_count")
				// 								->where('g.idPlayer = :idPlayer')
				// 								->groupBy('g.idPlayer')
				// 								->setParameter('idPlayer', $id)
				// 								->getQuery();
				// }
			}
		}
			//$query=$qb->getQuery()->getResult();
		
		
			if($request->request->get('select')=="csv")
			{
				
				$filename = "export_".date("Y_m_d_His").".csv";				
				$response=new Response();
				$response->setStatusCode(200);
				$response->headers->set('Content-Type', 'text/csv');
				$response->headers->set('Content-Description', 'Submissions Export');
				$response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
				$response->headers->set('Content-Transfer-Encoding', 'binary');
				$response->headers->set('Pragma', 'no-cache');
				$response->headers->set('Expires', '0');
				$out = fopen("php://temp", 'w');
				$data = stream_get_contents($out);
				fputcsv($out,$data);
				rewind($out); 				
				return $response; 	
			}
		
		
			else
			{
				return $this->render('gentelella.html.twig', array(
					'entities' =>$Results,
					'mapped'=>json_encode($lookup),	
					'load'=>$loadingTemplate,
					'Count'=>$Count,
				));
			}
	
	}
	
	public function topologicalsort($SelectedArray,$entities)
	{
		$Copy_entities=$entities;
		$List=array();
		$Remaining=array();
		//Without Incoming Edges- WI_E
		$WI_E=$entities;
			foreach($entities as $e1)
			{
				if($this->HasIncomingEdges($entities,$e1,$SelectedArray))
				{
					$key=array_search($e1,$WI_E);
					unset($WI_E[$key]);
				}
				
			}
			$Remaining=array_diff($Copy_entities,$WI_E);
			while(count($WI_E)>0)
			{
				$n=array_shift($WI_E);
				$List[]=$n;
				foreach($SelectedArray[$n] as $a => $m)
				{
					if(!$this->HasIncomingEdges($Remaining,$m,$SelectedArray))
					{
						$WI_E[]=$m;
					}
				}
			}
			return $List;
	}
	
	public function HasIncomingEdges($entities,$target,$Graph)
	{
		foreach($entities as $e)
				{
					if(in_array($target,$Graph[$e]))
					{
						return true;
					}
				}
			return false;
	}
	
	public function groups($query, $attribute, $SelectedArray,$group, $entity, $Alias)
	{
			$target=$SelectedArray[$entity][$attribute];
			if($Visited[$target])
			{
				return;
			}
			$temp=$lookup[$entity][$attribute]['SourceEntity'];
			$query->join($Alias,$attribute,$entity.'_'.$attribute,$attribute.$temp=$temp.'.id');
			$Visited[$target]=true;
			foreach($group as $g)
			{
				if($target==$g)
				{
					$query->addGroupBy($Alias.'.id');
					
				}
			}
			foreach($SelectedArray[$target] as $attr_tar=>$target_ent)
			{
					groups($query,$attr_tar,$SelectedArray,$group,$target,$entity.'_'.$attribute);
			}
	}
	public function load()
	{
		$conn = $this->getDoctrine()
					 ->getRepository(Template::class)
					 ->findAll();
		$loadval=$conn;
		return $loadval;
	}
	
	/**
     * @Route("/ajax/save_template", name="save_template")
     */
	function saveTemplateAction(Request $request) {
		if (!$request->isXmlHttpRequest()) 
		{
			throw new MethodNotAllowedException();
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$name = $request->request->get('name');
		$options = $request->request->get('string');
		$override = $request->request->get('override');
		
		if ($override == 'true') 
		{
			$event = $this->getDoctrine()
				   ->getRepository(Template::class)
				   ->findOneBy(array('name' => $name));
		} 
		else 
		{
			$event = new Template();
		}
		
		$event->setName($name);
		$event->setOptions($options);


		$em->persist($event);
		$em->flush();
		return new Response('Success!', 200);
	}
	/**
     * @Route("/ajax/loadTemplate", name="loadTemplate")
     */
	function loadTemplateAction(Request $request)
	{
		$name=$request->request->get('id');
		$conn = $this->getDoctrine()->getRepository(Template::class)->find($name);
		$loadval=$conn->getOptions();
		$response = new Response($loadval);
		$response->headers->set('Content-Type', 'application/json', 'charset=utf-8');
		return $response;
	}
	
	
	public function Metadata($class)
	{
		$class='\\'.$class;
		$m = $this->getDoctrine()
								->getManager()
								->getMetadataFactory()
								->getAllMetadata();
		foreach($m as $c){
			$n=$c->getName();
			if(substr($n,-strlen($class))==$class){
				return $n;				
			}

		}
		return null;
	}
	
	public function getRelatedEntities($ent_class) {
			$em = $this->getDoctrine()->getManager();
			$class = $em->getClassMetadata($ent_class);
			$all_meta = $em->getMetadataFactory()->getAllMetadata();
			$class_name = $class->getReflectionClass()->getName();
			foreach($all_meta as $ent) {
				/**
				 * @var $ent \Doctrine\ORM\Mapping\ClassMetadata
				 */
				$mappings = $ent->getAssociationMappings();
				$fks = [];
				foreach($mappings as $key => $field) {
					if (/*$field['targetEntity'] == $class_name ||*/ $field['sourceEntity'] == $class_name /*&& $field['isOwningSide'] == true*/) {
						
						$fks[] = $field;
					}
				}
				if (!empty($fks)) {
					yield $ent->getName() => $fks;
				}
			}
		}
		
		
	public function actionLookUpTable()
	{				
		$MappedData=array();
		
		$em = $this->getDoctrine()->getManager();
				
					$metadata=$em->getMetadataFactory()->getAllMetadata();

					$choices = [];
					foreach($metadata as $classMeta) {
						$choices[] = $classMeta->getName(); // Entity FQCN
					}
								
					foreach($choices as $choice){
						$pos=strrpos($choice,'\\');
						$EntityName=substr($choice,$pos+1);
						$MappedData[$EntityName]=array();
						
							
				foreach($this->getRelatedEntities($choice) as $related)
				{	
					foreach($related as $r) {

					$field = $r['fieldName'];
					$target=$r['targetEntity'];
					$source=$r['sourceEntity'];
					$pos=strrpos($target,'\\');
					$targetName=substr($target,$pos+1);
					$pos1=strrpos($source,'\\');
					$sourceName=substr($source,$pos1+1);
					if($field) {
					$MappedData[$EntityName][$field]=array(
					'FieldName'=>$field,
					'TargetEntity'=>$targetName,
					'SourceEntity'=>$sourceName,
					
					);
				}
				}
				}
			}
			return $MappedData;				
	}
}

