<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use App\Entity\User\User;
use App\Entity\Misc\Template;
use App\Entity\Misc\Room;



class ReportGeneratorController extends Controller {
	private $visited=array();
    /**
     * @Route("/" , name="default")
     */
    public function reportQueries(Request $request) {
		// This function contains all the query buildng functionalities for the report generator 
		$selectQuery=null;
		$output=array();
		$rel=array();
		$rows=array();
		$order=array();
		//Create a lookup table for the available entites that displays the target entites, source entity and the field name
		$lookup=$this->actionLookUpTable();
		$loadingTemplate=$this->load();
		$selectedArray=array();
		$selectedAttributes=array();
		$group=array();
		$results=array();
		$count=array();

		if($request->getMethod()=='POST'){
		
			//'GET','POST'
		
			$entities = $request->request->get('Entities');
			// When any GroupBy value is selected then do the following 
			if($request->request->get('GroupBy')!=Null)
			{
                foreach($entities as $e)
                {
                    $selectedAttributes = $request->request->get($e.'_Attributes');
                    $selectedArray[$e]=array();	
                    foreach($selectedAttributes as $a)
                    {
                        if(!empty($lookup[$e][$a])){
                        $selectedArray[$e][$a]=$lookup[$e][$a]['TargetEntity'];
                        }
                    }
                }
		
                foreach($selectedArray as $e=> $attr)
                {
                        foreach($attr as $a=>$target)
                        {
                            if(in_array($e,$selectedArray[$target]))
                            {
                                $key=array_search($e,$selectedArray[$target]);
                                unset($selectedArray[$target][$key]);	
                            }
                        }
				}
				//Call to the topologicalsort function which is defined after this function
			    $sorted=$this->topologicalsort($selectedArray,$entities);
			
                $group=$request->request->get('GroupBy');
                $conn= $this->getDoctrine()->getManager()->getConnection();
                $qb=$conn->createQueryBuilder();
                foreach($sorted as $entity)
                {
                    $attr=$selectedArray[$entity];
                    foreach($attr as $attribute){
                    $qb=$this->addSelect($attribute,$entity.'.$attribute')
                            ->from($entity,$entity);
                    }
                    foreach($attr as $a=>$target)
                    {
                        $qb=$this->groups($qb,$a,$selectedArray,$group, $entity,$entity);				
                    }
                    $output=$qb->execute()->fetchAll();	
                }
                foreach($output as $query){
                    $results[]=array(
                        'entity'=>$query,
                        'attributes'=>$attr,
                        );	
                }
			}
			//If groupBy is not selected, then do the following
			else
			{
				// If the Aggreagtion value is not selected then do the following
				if(!($request->request->get('Aggregation')))
				{
					foreach($entities as $entity)
					{
                        $selectedAttributes = $request->request->get($entity.'_Attributes');
                        $conn= $this->getDoctrine()->getManager()->getConnection();
                        $qb=$conn->createQueryBuilder();
                        foreach($selectedAttributes as $attr)
                        {
                            $qb->addSelect($attr);
						}
						//Converting string to lowercase because entity value is defined that way
                        $qb=$qb->from(strtolower($entity)); 
                        $selectQuery=$qb->execute()->fetchAll();		
					}
				}
				//If the aggregation value is selected and it is count, then do the following (Currently defined just for one entity)
				else if(($request->request->get('Aggregation'))=='Count')
				{
					$em = $this->getDoctrine()->getManager();	
					$query = $em->createQuery('SELECT SUM(r.number), r.number FROM App\Entity\Misc\Room r GROUP BY r.number');
					$qb=$query->getResult();
					$count[]=array(
						'Count'=>$qb,		
					);	
				}
				//If aggregation value is selected and it is Sum, then do the following (Currently defined just for one entity)
				else if(($request->request->get('Aggregation'))=='Sum')
				{
					$em = $this->getDoctrine()->getManager();
					
					$query = $em->createQuery('SELECT SUM(r.capacity), r.number FROM App\Entity\Misc\Room r GROUP BY r.number');
					
					$qb=$query->getResult();
					$count[]=array(
						'Count'=>$qb,	
			    	);
			    }
			}
		}
		//If the request is not passed as a POST method, then do the following
		//If the button pressed is Generate CSV then do the following
		if($request->request->get('select')=="csv")
	    {
				
            $filename = "export_".date("Y_m_d_His").".csv";				
            $response=new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'text/csv');
            $response->headers->set('Content-Description', 'Submissions Export');
            $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
            $response->headers->set('Content-Transfer-Encoding', 'binary');
            $response->headers->set('Pragma', 'no-cache');
			$response->headers->set('Expires', '0');
			//The following code is to insert data generated by the query into the csv file that gets created
            $rows=array();
            $rows[]=implode($selectedAttributes,',');
            foreach($selectQuery as $row)
            {
                $rows[]=implode($row,',');                    
            }		
            $csv=implode($rows,"\n");
            $response->setContent($csv);
            return $response; 	
		}
		//Render all the values to the Twig file
		else
        {
            return $this->render('report_generator.html.twig', array(
                'entities' =>$selectQuery,
                'attributes'=>$selectedAttributes,
                'mapped'=>json_encode($lookup),	
                'load'=>$loadingTemplate,
                'Count'=>$count,
            ));
        }
	
    }
    //Topological sort to order the multiple entities selected, inorder to perform the groupBy function.
	public function topologicalsort($selectedArray,$entities)
	{
		$copyEntities=$entities;
		$list=array();
		$remaining=array();
		//Without Incoming Edges- wie
		$wie=$entities;
			foreach($entities as $e1)
			{
				//Incoming edges denote the dependency
				//If there are incoming edges to the particular entity and if it is bidirectional, then remove the dependency (below if statement shows that)
				if($this->hasIncomingEdges($entities,$e1,$selectedArray))
				{
					$key=array_search($e1,$wie);
					unset($wie[$key]);
				}
				
			}
			//Copy the remaining with incoming edges into this array
			$remaining=array_diff($copyEntities,$wie);
			while(count($wie)>0)
			{
				$n=array_shift($wie);
				$list[]=$n;
				foreach($selectedArray[$n] as $a => $m)
				{
					if(!$this->hasIncomingEdges($remaining,$m,$selectedArray))
					{
						$wie[]=$m;
					}
				}
			}
			//returns a list that has entities that have been ordered based on the incoming edges
			return $list;
	}
	//Checks if that particular entity($entities) being passed has any dependency ($target), $graph is an array that has to be checked
	public function hasIncomingEdges($entities,$target,$graph)
	{
		foreach($entities as $e)
        {
            if(in_array($target,$graph[$e]))
            {
                return true;
            }
        }
		return false;
	}
	//groups function has a recursive function that is used to create the query to be used when a GroupBy entity is actually selected in the front end
	public function groups($query, $attribute, $selectedArray,$group, $entity, $Alias)
	{
        $target=$selectedArray[$entity][$attribute];
        if($visited[$target])
        {
            return;
        }
        $temp=$lookup[$entity][$attribute]['SourceEntity'];
        $query->join($Alias,$attribute,$entity.'_'.$attribute,$attribute.$temp=$temp.'.id');
        $visited[$target]=true;
        foreach($group as $g)
        {
            if($target==$g)
            {
                $query->addGroupBy($Alias.'.id');
                
            }
        }
        foreach($selectedArray[$target] as $attr_tar=>$target_ent)
        {
                groups($query,$attr_tar,$selectedArray,$group,$target,$entity.'_'.$attribute);
        }
    }
    //The load() is used to retrieve the templates that are being saved from the UI to display in the dropdown
	public function load()
	{
		$conn = $this->getDoctrine()
					 ->getRepository(Template::class)
					 ->findAll();
		$loadval=$conn;
		return $loadval;
	}
	
	/**
     * @Route("/ajax/save_template", name="save_template")
     */
	function saveTemplateAction(Request $request) {
		//Save template is used to get all the selected values from the UI and save them in a json format in the database
		if (!$request->isXmlHttpRequest()) 
		{
			throw new MethodNotAllowedException();
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$name = $request->request->get('name');
		$options = $request->request->get('string');
		$override = $request->request->get('override');
		
		if ($override == 'true') 
		{
			$event = $this->getDoctrine()
				   ->getRepository(Template::class)
				   ->findOneBy(array('name' => $name));
		} 
		else 
		{
			$event = new Template();
		}
		
		$event->setName($name);
		$event->setOptions($options);


		$em->persist($event);
		$em->flush();
		return new Response('Success!', 200);
	}
	/**
     * @Route("/ajax/loadTemplate", name="loadTemplate")
     */
	function loadTemplateAction(Request $request)
	{
		//loadTemplateAction() is used when a value is selected from the load dropdown to populate the UI with pre set selections
		$name=$request->request->get('id');
		$conn = $this->getDoctrine()->getRepository(Template::class)->find($name);
		$loadval=$conn->getOptions();
		$response = new Response($loadval);
		$response->headers->set('Content-Type', 'application/json', 'charset=utf-8');
		return $response;
	}
	
	//Used to retrieve the background information of each entity that is being passed
	public function Metadata($class)
	{
		$class='\\'.$class;
		$m = $this->getDoctrine()
								->getManager()
								->getMetadataFactory()
								->getAllMetadata();
		foreach($m as $c){
			$n=$c->getName();
			
            if(substr($n,-strlen($class))==$class)
            {
				return $n;				
			}

		}
		return null;
	}
	//The getRelatedEntites() is used to retrieve the relationships between various entities from doctrine
	public function getRelatedEntities($ent_class) {
			$em = $this->getDoctrine()->getManager();
			$class = $em->getClassMetadata($ent_class);
			$all_meta = $em->getMetadataFactory()->getAllMetadata();
			$class_name = $class->getReflectionClass()->getName();
            foreach($all_meta as $ent) 
            {
				/**
				 * @var $ent \Doctrine\ORM\Mapping\ClassMetadata
				 */
				$mappings = $ent->getAssociationMappings();
				$fieldKeys = [];
				foreach($mappings as $key => $field) {
					if ( $field['sourceEntity'] == $class_name ) {
						
						$fieldKeys[] = $field;
					}
				}
				if (!empty($fieldKeys)) {
					yield $ent->getName() => $fieldKeys;
				}
			}
		}
		
	//actionLookUpTable() is used to retrieve the background information like the Target Entity value, Source entity value, and Field of all the entities that are registered in doctrine
	public function actionLookUpTable()
	{				
		$mappedData=array();
		
		$em = $this->getDoctrine()->getManager();
				
        $metadata=$em->getMetadataFactory()->getAllMetadata();

        $choices = [];
        foreach($metadata as $classMeta)
        {
            $choices[] = $classMeta->getName(); // Entity FQCN
        }
								
        foreach($choices as $choice)
        {
            $pos=strrpos($choice,'\\');
            $entityName=substr($choice,$pos+1);
            $mappedData[$entityName]=array();
            foreach($this->getRelatedEntities($choice) as $related)
            {	
                foreach($related as $r) 
                {

                    $field = $r['fieldName'];
                    $target=$r['targetEntity'];
					$source=$r['sourceEntity'];
					//Below lines are to get just the name and not the entire path of the target and source entities
                    $pos=strrpos($target,'\\');
                    $targetName=substr($target,$pos+1);
                    $pos1=strrpos($source,'\\');
                    $sourceName=substr($source,$pos1+1);
                    if($field) {
                        $mappedData[$entityName][$field]=array(
                        'FieldName'=>$field,
                        'TargetEntity'=>$targetName,
                        'SourceEntity'=>$sourceName,
                        );
                    }
                }
            }
        }
		return $mappedData;				
	}
}

